# Studiu pentru predicția accidentelor vasculare folosind învățarea automată

## Descriere

AVCPredictor este o aplicație Python bazată pe Jupyter Notebook care demonstrează integrarea unui model Core ML generat în Python într-o aplicație iOS. Aplicația demonstrează o abordare generică pentru implementarea unui model de învățare automată într-o aplicație mobilă.

## Caracteristici

- Generarea și exportul unui model Core ML în Python
- Utilizarea modelului în aplicația iOS pentru a realiza predicții
- Demonstrarea integrării între aplicația iOS și Python

## Configurare

1. Clonează sau descarcă acest repository.

2. Asigură-te că ai instalat Python și Jupyter Notebook.

3. Deschide Jupyter Notebook utilizând comanda `jupyter notebook` în terminal.

4. Deschide fișierul `Stroke-LogisticRegression.ipynb` în Jupyter Notebook.

5. Execută fiecare celulă a notebook-ului în ordine pentru a rula aplicația și a genera modelul Core ML.

6. Exportă modelul Core ML generat în directorul corespunzător pentru integrarea în aplicația iOS.

## Integrarea în aplicația iOS

1. Asigură-te că ai instalat cea mai recentă versiune de Xcode.

2. În directorul proiectului iOS, deschide fișierul `StrokeApp.xcodeproj` în Xcode.

3. Asigură-te că modelul Core ML generat în Python este prezent în structura proiectului. Dacă nu este prezent, importă-l în directorul corespunzător.

4. În codul sursă al aplicației iOS, localizează secțiunea corespunzătoare utilizării modelului Core ML și ajustează parametrii necesari, cum ar fi numele și tipul de intrare/ieșire al modelului.

5. Compilează și rulează aplicația pe un simulator sau pe un dispozitiv iOS pentru a utiliza modelul Core ML în aplicație.

## Autor

Maranescu Iulia-Andreea 
