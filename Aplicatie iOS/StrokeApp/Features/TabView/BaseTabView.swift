//
//  BaseTabView.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Resolver
import Combine

struct BaseTabView: View {

    enum TabItemIndices: Int, CaseIterable {
        case home = 0
        case predict = 1
        case menu = 2
    }

    // MARK: - Body

    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            tabView
        }
    }

    // MARK: - Private

    private var tabView: some View {
        GeometryReader { geometry in
            ZStack(alignment: .bottomLeading) {
                simpleTabView
            }
        }
    }

    private var simpleTabView: some View {
        TabView(selection: .constant(0)) {
            homeScreen
                .tabItem {
                    Label(
                        "Acasă",
                        systemImage: "house.fill"
                    )
                }
                .tag(TabItemIndices.home.rawValue)

            predictScreen
                .tabItem {
                    Label(
                        "Predicție",
                        systemImage: "questionmark.circle.fill"
                    )
                }
                .tag(TabItemIndices.predict.rawValue)

            menuScreen
                .tabItem {
                    Label(
                        "Meniu",
                        systemImage: "menucard.fill"
                    )
                }
                .tag(TabItemIndices.menu.rawValue)
        }
    }


    private var homeScreen: some View {
        HomeScreen()
    }

    private var predictScreen: some View {
        PredictScreen()
    }

    private var menuScreen: some View {
        MenuScreen()
    }
}
