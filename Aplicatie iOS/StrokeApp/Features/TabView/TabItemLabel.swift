//
//  TabItemLabel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI

struct TabItemLabel: View {

    // MARK: - Properties

    let textKey: String

    let imageKey: String

    private let tabItemWidth: CGFloat = 25.0

    // MARK: - Body

    var body: some View {
        Label {
            Text(textKey)
        } icon: {
            Image(imageKey)
                .renderingMode(.template)
                .frame(width: tabItemWidth, height: tabItemWidth)
        }
    }
}
