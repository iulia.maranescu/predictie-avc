//
//  MenuViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import CoreML
import SwiftUI
import Resolver

class MenuViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var authenticationService: AuthenticationService
}

// MARK: - Public

extension MenuViewModel {

    func onLogOut() {
        authenticationService.deauthenticate()
    }
}
