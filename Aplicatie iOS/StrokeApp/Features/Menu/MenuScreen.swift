//
//  MenuScreen.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import SwiftUI
import Resolver

struct MenuScreen: View {

    // MARK: - Dependencies

    @StateObject private var viewModel: MenuViewModel = Resolver.resolve()

    // MARK: - Body

    var body: some View {
        NavigationView {
            content
                .navigationTitle("Meniu")
                .navigationBarTitleDisplayMode(.large)
        }
    }

    private var content: some View {
        VStack(alignment: .center, spacing: 16) {
            HTMLStringView(htmlContent: HTMLTextProvider().provideMenuText())
            Spacer()
            logOutButton
            Spacer().frame(height: 20)
        }
        .padding(.horizontal, 16)
    }

    private var logOutButton: some View {
        Button(
            action: {
                viewModel.onLogOut()
            }, label: {
                Text("Deconectare").bold()
            }
        )
        .frame(width: 120)
        .padding()
        .background(.black)
        .foregroundColor(.white)
        .clipShape(Capsule())
    }
}

