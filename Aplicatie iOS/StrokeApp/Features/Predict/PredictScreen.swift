//
//  PredictScreen.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import SwiftUI
import Resolver

struct PredictScreen: View {

    // MARK: - Dependencies

    @StateObject private var viewModel: PredictViewModel = Resolver.resolve()

    // MARK: - Body

    var body: some View {
        NavigationView {
            if viewModel.hasSubmitted {
                VStack(alignment: .center, spacing: 16) {
                    Spacer()
                    Text("Vă mulțumim pentru completarea chestionarului, puteți verifica ecranul de start pentru a vedea cea mai recentă predicție!")
                        .bold()
                        .multilineTextAlignment(.center)
                    retryButton
                    Spacer()
                }
                .navigationTitle("Predicție")
                .navigationBarTitleDisplayMode(.large)
            } else {
                content
                    .navigationTitle("Predicție")
                    .navigationBarTitleDisplayMode(.large)
            }
        }
    }

    private var content: some View {
        ScrollView(showsIndicators: false) {
            VStack(alignment: .center, spacing: 16) {
                Text("Vă rugăm să completați următoarele date").bold().font(.title2)
                Divider()
                formularView
                predictButton
                Spacer().frame(height: 50)
            }
            .padding(.horizontal, 16)
        }
    }

    private var formularView: some View {
        VStack(alignment: .leading, spacing: 16) {
            ageView
            bmiView
            glucoseLevelView
            smokingStatusView
            workStatusView
            residenceStatusView
            genderStatusView
            hypertensionStatusView
            heartDiseaseStatusView
        }
    }

    private var ageView: some View {
        VStack(alignment: .leading) {
            Text("Vârstă").bold()
            TextField("Vârstă", value: $viewModel.age, format: .number)
                .keyboardType(.numberPad)
                .textFieldStyle(.roundedBorder)
            if viewModel.ageError {
                Text("Vârsta trebuie să fie între 18 și 99 de ani").foregroundColor(.red)
            }
            Divider()
        }
    }

    private var bmiView: some View {
        VStack(alignment: .leading) {
            Text("IMC").bold()
            TextField("IMC", value: $viewModel.bmi, format: .number)
                .keyboardType(.numberPad)
                .textFieldStyle(.roundedBorder)
            if viewModel.bmiError {
                Text("Valoarea IMC trebuie să fie între 10 și 50").foregroundColor(.red)
            }
            Divider()
        }
    }

    private var glucoseLevelView: some View {
        VStack(alignment: .leading) {
            Text("Nivelul de glucoză").bold()
            TextField("Nivelul de glucoză", value: $viewModel.glucoseLevel, format: .number)
                .keyboardType(.numberPad)
                .textFieldStyle(.roundedBorder)
            if viewModel.glucoseError {
                Text("Valoarea nivelului de glucoză trebuie să fie între 50 și 250").foregroundColor(.red)
            }
            Divider()
        }
    }

    private var smokingStatusView: some View {
        VStack(alignment: .leading) {
            Text("Statutul de fumător").bold()
            Picker("Selectași statutul de fumător", selection: $viewModel.smokingStatus) { // 3
                ForEach(SmokingStatus.allCases, id: \.self) { item in
                    Text(item.description)
                }
            }
            Divider()
        }
    }

    private var workStatusView: some View {
        VStack(alignment: .leading) {
            Text("Statutul profesional").bold()
            Picker("Selectati statutul profesional", selection: $viewModel.workingStatus) { // 3
                ForEach(WorkStatus.allCases, id: \.self) { item in
                    Text(item.description)
                }
            }
            Divider()
        }
    }

    private var residenceStatusView: some View {
        VStack(alignment: .leading) {
            Text("Statutul de rezidență").bold()
            Picker("Selectați statutul de rezidență", selection: $viewModel.residenceStatus) { // 3
                ForEach(ResidenceStatus.allCases, id: \.self) { item in
                    Text(item.description)
                }
            }
            Divider()
        }
    }

    private var genderStatusView: some View {
        VStack(alignment: .leading) {
            Text("Gen").bold()
            Picker("Selectați genul", selection: $viewModel.genderStatus) { // 3
                ForEach(GenderStatus.allCases, id: \.self) { item in
                    Text(item.description)
                }
            }
            Divider()
        }
    }

    private var hypertensionStatusView: some View {
        VStack(alignment: .leading) {
            Text("Aveți probleme cu hipertensiunea arterială?").bold()
            Picker("Selectați statutul de hipertensiune", selection: $viewModel.hypertensionStatus) { // 3
                ForEach(HypertensionStatus.allCases, id: \.self) { item in
                    Text(item.description)
                }
            }
            Divider()
        }
    }

    private var heartDiseaseStatusView: some View {
        VStack(alignment: .leading) {
            Text("Aveți probleme cu inima?").bold()
            Picker("Selectați starea bolii de inimă", selection: $viewModel.heartDiseaseStatus) { // 3
                ForEach(HeartDiseaseStatus.allCases, id: \.self) { item in
                    Text(item.description)
                }
            }
            Divider()
        }
    }

    private var predictButton: some View {
        Button(
            action: {
                viewModel.predictStroke()
            }, label: {
                Text("Predicție").bold()
            }
        )
        .disabled(!viewModel.isButtonEnabled())
        .opacity(viewModel.isButtonEnabled() ? 1.0 : 0.3)
        .frame(width: 100)
        .padding()
        .background(.black)
        .foregroundColor(.white)
        .clipShape(Capsule())
    }

    private var retryButton: some View {
        Button(
            action: {
                viewModel.hasSubmitted = false
            }, label: {
                Text("Trimite din nou").bold()
            }
        )
        .frame(width: 160)
        .padding()
        .background(.black)
        .foregroundColor(.white)
        .clipShape(Capsule())
    }
}
