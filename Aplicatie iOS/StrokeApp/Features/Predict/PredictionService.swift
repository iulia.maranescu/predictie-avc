//
//  PredictionService.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import Combine
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

protocol PredictionService {

    func clearPredictions() -> Future<Void, Error>

    func getPredictions() -> Future<[PredictionModel], Error>

    func addPrediction(prediction: PredictionModel) -> Future<Void, Error>
}

struct RealPredictionService: PredictionService {

    // MARK: - PredictionService

    func getPredictions() -> Future<[PredictionModel], Error> {
        Future<[PredictionModel], Error> { promise in
            Firestore.firestore()
                .collection("predictions")
                .whereField("userId", isEqualTo: Auth.auth().currentUser?.uid ?? "")
                .order(by: "date", descending: true)
                .getDocuments { (querySnapshot, error) in
                if let error = error {
                    promise(.failure(error))
                } else {
                    guard let documents = querySnapshot?.documents else { return }
                    promise(.success(documents.compactMap { queryDocumentSnapshot -> PredictionModel? in
                        return try? queryDocumentSnapshot.data(as: PredictionModel.self)
                    }))
                }
            }
        }
    }

    func addPrediction(prediction: PredictionModel) -> Future<Void, Error> {
        return Future<Void, Error> { promise in
            Firestore.firestore()
                .collection("predictions")
                .addDocument(
                    data: [
                        "userId": Auth.auth().currentUser?.uid ?? "",
                        "age": prediction.age,
                        "hyperTension": prediction.hyperTension,
                        "heartDisease": prediction.heartDisease,
                        "glucoseLevel": prediction.glucoseLevel,
                        "bmi": prediction.bmi,
                        "smokingStatus": prediction.smokingStatus,
                        "workType": prediction.workType,
                        "residence": prediction.residence,
                        "gender": prediction.gender,
                        "prediction": prediction.prediction,
                        "date": Timestamp()
                    ]
                ) { error in
                    if let error = error {
                        promise(.failure(error))
                    } else {
                        promise(.success(()))
                    }
                }
        }
    }

    func clearPredictions() -> Future<Void, Error> {
        return Future<Void, Error> { promise in
            Firestore.firestore()
                .collection("predictions")
                .whereField("userId", isEqualTo: Auth.auth().currentUser?.uid ?? "")
                .getDocuments { (querySnapshot, error) in
                    if let error = error {
                        promise(.failure(error))
                    } else {
                        guard let documents = querySnapshot?.documents else { return }
                        for document in documents {
                            document.reference.delete()
                        }
                        promise(.success(()))
                    }
                }
        }
    }
}
