//
//  PredictViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import CoreML
import SwiftUI
import Combine
import Resolver
import FirebaseFirestore

class PredictViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var predictionService: PredictionService

    // MARK: - Properties

    @Published var hasSubmitted: Bool = false

    @Published var glucoseLevel: Double = 100

    @Published var glucoseError = false

    @Published var age: Double = 50

    @Published var ageError = false

    @Published var bmi: Double = 20

    @Published var bmiError = false

    @Published var smokingStatus: SmokingStatus = .formerlySmoked

    @Published var workingStatus: WorkStatus = .selfEmployed

    @Published var residenceStatus: ResidenceStatus = .urban

    @Published var genderStatus: GenderStatus = .other

    @Published var hypertensionStatus: HypertensionStatus = .no

    @Published var heartDiseaseStatus: HeartDiseaseStatus = .no

    var cancellables: Set<AnyCancellable> = []

    // MARK: - Init

    init() {
        subscribeToPublishers()
    }
}

// MARK: - Public

extension PredictViewModel {

    func predictStroke() {
        let model = StrokePrediction()

        do {
            let prediction = try model.prediction(
                age: age,
                hypertension: hypertensionStatus.rawValue,
                heart_disease: heartDiseaseStatus.rawValue,
                avg_glucose_level: glucoseLevel,
                bmi: bmi,
                smoking_status_numerical: smokingStatus.rawValue,
                work_type_numerical: workingStatus.rawValue,
                Residence_type_numerical: residenceStatus.rawValue,
                gender_numerical: genderStatus.rawValue
            )
            uploadPrediction(prediction: Bool(truncating: prediction.stroke as NSNumber))
        } catch {

        }
    }

    func isButtonEnabled() -> Bool {
        !ageError && !bmiError && !glucoseError
    }
}

// MARK: - Private

private extension PredictViewModel {

    func subscribeToPublishers() {
        $glucoseLevel
            .sink(receiveValue: { [weak self] in
            if $0 > 250 || $0 < 50 {
                self?.glucoseError = true
            } else {
                self?.glucoseError = false
            }
        })
        .store(in: &cancellables)

        $age
            .sink(receiveValue: { [weak self] in
            if $0 > 99 || $0 < 18 {
                self?.ageError = true
            } else {
                self?.ageError = false
            }
        })
        .store(in: &cancellables)

        $bmi
            .sink(receiveValue: { [weak self] in
            if $0 > 50 || $0 < 10 {
                self?.bmiError = true
            } else {
                self?.bmiError = false
            }
        })
        .store(in: &cancellables)
    }

    func uploadPrediction(prediction: Bool) {
        _ = predictionService.addPrediction(
            prediction: .init(
                userId: "",
                age: age,
                hyperTension: hypertensionStatus.rawValue,
                heartDisease: heartDiseaseStatus.rawValue,
                glucoseLevel: glucoseLevel,
                bmi: bmi,
                smokingStatus: smokingStatus.rawValue,
                workType: workingStatus.rawValue,
                residence: residenceStatus.rawValue,
                gender: genderStatus.rawValue,
                prediction: prediction,
                date: Timestamp()
            )
        )

        self.hasSubmitted = true
    }
}
