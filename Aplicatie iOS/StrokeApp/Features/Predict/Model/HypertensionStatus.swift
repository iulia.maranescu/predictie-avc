//
//  HypertensionStatus.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

enum HypertensionStatus: Double, CaseIterable {

    case no = 0

    case yes = 1

    var description: String {
        switch self {
        case .no:
            return "Nu"
        case .yes:
            return "Da"
        }
    }
}
