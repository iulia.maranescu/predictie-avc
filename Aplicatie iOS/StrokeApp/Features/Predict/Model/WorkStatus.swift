//
//  WorkStatus.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

enum WorkStatus: Double, CaseIterable {

    case govJob = 0

    case neverWorked = 1

    case privateSector = 2

    case selfEmployed = 3

    case children = 4

    var description: String {
        switch self {
        case .govJob:
            return "Sector public"
        case .neverWorked:
            return "Nu ați avut vreodată un loc de muncă"
        case .privateSector:
            return "Sectorul privat"
        case .selfEmployed:
            return "Liber profesionist"
        case .children:
            return "Copil"
        }
    }
}
