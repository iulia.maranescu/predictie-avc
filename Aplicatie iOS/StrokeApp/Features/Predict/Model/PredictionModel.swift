//
//  PredictionModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

struct PredictionModel: Codable, Equatable {

    let uid = UUID().uuidString

    let userId: String

    let age: Double

    let hyperTension: Double

    let heartDisease: Double

    let glucoseLevel: Double

    let bmi: Double

    let smokingStatus: Double

    let workType: Double

    let residence: Double

    let gender: Double

    let prediction: Bool

    let date: Timestamp
}
