//
//  GenderStatus.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

enum GenderStatus: Double, CaseIterable {

    case female = 0

    case male = 1

    case other = 2

    var description: String {
        switch self {
        case .female:
            return "Feminin"
        case .male:
            return "Masculin"
        case .other:
            return "Altul"
        }
    }
}
