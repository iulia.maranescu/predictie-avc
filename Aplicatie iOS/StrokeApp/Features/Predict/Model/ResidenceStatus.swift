//
//  ResidenceStatus.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

enum ResidenceStatus: Double, CaseIterable {

    case rural = 0

    case urban = 1

    var description: String {
        switch self {
        case .rural:
            return "Rural"
        case .urban:
            return "Urban"
        }
    }
}
