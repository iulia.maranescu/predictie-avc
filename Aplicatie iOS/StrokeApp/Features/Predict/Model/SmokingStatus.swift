//
//  SmokingStatus.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

enum SmokingStatus: Double, CaseIterable {

    case unknown = 0

    case formerlySmoked = 1

    case neverSmoked = 2

    case smokes = 3

    var description: String {
        switch self {
        case .unknown:
            return "Necunoscut"
        case .formerlySmoked:
            return "Fost fumător"
        case .neverSmoked:
            return "Nefumător"
        case .smokes:
            return "Fumător"
        }
    }
}
