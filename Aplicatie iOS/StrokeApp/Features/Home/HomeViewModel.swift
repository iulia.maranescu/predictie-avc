//
//  HomeViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import CoreML
import SwiftUI
import Combine
import Resolver

class HomeViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var authenticationService: AuthenticationService

    @Injected private var predictionService: PredictionService

    // MARK: - Properties

    @Published var predictions: [PredictionModel] = []

    @Published var username: String = "Anonymous"

    private var cancellables: Set<AnyCancellable> = []
}

// MARK: - Public

extension HomeViewModel {

    func onViewAppear() {
        getPredictions()
        getUsername()
    }

    func deletePredictions() {
        predictionService
            .clearPredictions()
            .sink(receiveCompletion: { [weak self] _ in self?.getPredictions() }, receiveValue: { _ in })
            .store(in: &cancellables)
    }
}

// MARK: - Private

private extension HomeViewModel {

    func getPredictions() {
        predictionService
            .getPredictions()
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] predictions in
                    self?.predictions = predictions
                }
            )
            .store(in: &cancellables)
    }

    func getUsername() {
        authenticationService
            .currentUser()
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] user in
                    self?.username = user?.email ?? "Anonymous"
                }
            )
            .store(in: &cancellables)
    }
}

