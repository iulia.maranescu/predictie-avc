//
//  HomeScreen.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Resolver

struct HomeScreen: View {

    // MARK: - Dependencies

    @StateObject private var viewModel: HomeViewModel = Resolver.resolve()

    // MARK: - Body

    var body: some View {
        NavigationView {
            content
                .navigationTitle("Predicțiile mele")
                .navigationBarTitleDisplayMode(.large)
        }.onAppear {
            viewModel.onViewAppear()
        }
    }

    private var content: some View {
        VStack(alignment: .leading, spacing: 16) {
            Text("Bun venit, \(viewModel.username)!").bold().font(.headline)
            if viewModel.predictions.isEmpty {
                emptyPredictionsView
            } else {
                predictionsView
            }
        }
        .padding(.horizontal, 16)
    }

    private var emptyPredictionsView: some View {
        VStack(alignment: .center) {
            Spacer()
            Text("Nu aveți nicio predicție, accesați ecranul de predicții și introduceți datele pentru a vă verifica starea de sănătate")
                .bold()
                .multilineTextAlignment(.center)
            Spacer()
        }
    }

    private var predictionsView: some View {
        ScrollView(showsIndicators: false) {
            LazyVStack {
                HStack {
                    Spacer()
                    deleteButton
                }.padding(.horizontal, 16)
                ForEach(viewModel.predictions, id: \.uid) { prediction in
                    predictionsCardView(prediction: prediction)
                        .padding(16)
                    Spacer().frame(height: 6)
                }
            }
        }
    }

    private func predictionsCardView(prediction: PredictionModel) -> some View {
        VStack(alignment: .leading) {
            formularView(prediction: prediction)
            Divider()
            predictionView(prediction: prediction)
            Spacer().frame(height: 8)
        }
        .background(
            Rectangle()
                .fill(Color.white)
                .cornerRadius(12)
                .shadow(color: Color.gray.opacity(0.7), radius: 8, x: 0, y: 0)
        )
    }

    private func formularView(prediction: PredictionModel) -> some View {
        VStack(alignment: .leading) {
            Spacer().frame(height: 8)
            VStack {
                dateView(prediction: prediction)
                ageView(prediction: prediction)
                smokeStatusView(prediction: prediction)
                hypertensionView(prediction: prediction)
            }
            heartDiseaseView(prediction: prediction)
            glucoseLevelView(prediction: prediction)
            bmiView(prediction: prediction)
            workTypeView(prediction: prediction)
            residenceTypeView(prediction: prediction)
            genderTypeView(prediction: prediction)
        }
    }

    private func dateView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Data")
            Spacer()
            Text(prediction.date.dateValue().formatted(date: .long, time: .shortened))
        }
        .padding(.horizontal, 16)
    }

    private func ageView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Vârsta")
            Spacer()
            Text(String(prediction.age))
        }
        .padding(.horizontal, 16)
    }

    private func hypertensionView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Hipertensiune")
            Spacer()
            Text(HypertensionStatus(rawValue: prediction.hyperTension)?.description ?? "")
        }
        .padding(.horizontal, 16)
    }

    private func heartDiseaseView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Boli de inimă")
            Spacer()
            Text(HeartDiseaseStatus(rawValue: prediction.heartDisease)?.description ?? "")
        }
        .padding(.horizontal, 16)
    }

    private func glucoseLevelView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Nivelul de glucoză")
            Spacer()
            Text(String(prediction.glucoseLevel))
        }
        .padding(.horizontal, 16)
    }

    private func bmiView(prediction: PredictionModel) -> some View {
        HStack {
            Text("IMC")
            Spacer()
            Text(String(prediction.bmi))
        }
        .padding(.horizontal, 16)
    }

    private func smokeStatusView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Statut de fumător")
            Spacer()
            Text(SmokingStatus(rawValue: prediction.smokingStatus)?.description ?? "")
        }
        .padding(.horizontal, 16)
    }

    private func workTypeView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Statut profesional")
            Spacer()
            Text(WorkStatus(rawValue: prediction.workType)?.description ?? "")
        }
        .padding(.horizontal, 16)
    }

    private func residenceTypeView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Statut de rezidență")
            Spacer()
            Text(ResidenceStatus(rawValue: prediction.residence)?.description ?? "")
        }
        .padding(.horizontal, 16)
    }

    private func genderTypeView(prediction: PredictionModel) -> some View {
        HStack {
            Text("Gen")
            Spacer()
            Text(GenderStatus(rawValue: prediction.gender)?.description ?? "")
        }
        .padding(.horizontal, 16)
    }

    private func predictionView(prediction: PredictionModel) -> some View {
        HStack {
            Spacer()
            Text(prediction.prediction ? "Vă rugăm să solicitați asistență medicală" : "Totul e în regulă")
            Spacer()
        }
        .padding(.horizontal, 16)
    }

    private var deleteButton: some View {
        Button(
            action: {
                viewModel.deletePredictions()
            }, label: {
                Text("Ștergeți predicțiile").bold()
            }
        )
        .frame(width: 200)
        .padding()
        .background(.red)
        .foregroundColor(.white)
        .clipShape(Capsule())
    }
}
