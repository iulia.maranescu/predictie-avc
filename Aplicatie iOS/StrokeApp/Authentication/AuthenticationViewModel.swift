//
//  AuthenticationViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Combine
import Resolver

class AuthenticationViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var authenticationService: AuthenticationService

    // MARK: - Properties

    @Published var presentRegister: Bool = false

    @Published var presentLogin: Bool = false

    private var cancellables: Set<AnyCancellable> = []
}

// MARK: - Public

extension AuthenticationViewModel {

    func onRegisterTap() {
        presentRegister = true
    }

    func onLoginTap() {
        presentLogin = true
    }
}
