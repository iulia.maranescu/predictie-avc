//
//  AuthenticationScreen.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Resolver

struct AuthenticationScreen: View {

    // MARK: - Dependencies

    @StateObject private var viewModel: AuthenticationViewModel = Resolver.resolve()

    // MARK: - Body

    var body: some View {
        NavigationView {
            ZStack {
                registerView
                loginView
                Image("LaunchImage")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                content
            }
            .navigationTitle("Bun venit")
            .navigationBarHidden(true)
            .frame(width: UIScreen.main.bounds.width)
            .edgesIgnoringSafeArea([.top, .bottom])
        }
    }

    private var content: some View {
        VStack(alignment: .center, spacing: 16) {
            Spacer().frame(height: 150)

            Text("Bun venit")
                .font(.largeTitle)
                .bold()
                .multilineTextAlignment(.center)
                .foregroundColor(.white)

            Spacer()

            HStack {
                registerButton
                loginButton
            }
            Spacer().frame(height: 100)
        }
        .padding(.horizontal, 16)
    }

    private var loginButton: some View {
        Button(
            action: {
                viewModel.onLoginTap()
            }, label: {
                Text("Conectare")
                    .bold()
            }
        )
        .frame(width: 100)
        .padding()
        .background(.white)
        .foregroundColor(.black)
        .clipShape(Capsule())
    }

    private var registerButton: some View {
        Button(
            action: {
                viewModel.onRegisterTap()
            }, label: {
                Text("Înregistrare")
                    .bold()
            }
        )
        .frame(width: 100)
        .padding()
        .background(.white)
        .foregroundColor(.black)
        .clipShape(Capsule())
    }

    private var registerView: some View {
        NavigationLink(
            isActive: $viewModel.presentRegister,
            destination: { RegisterView() },
            label: {
                Text("")
            }
        )
    }

    private var loginView: some View {
        NavigationLink(
            isActive: $viewModel.presentLogin,
            destination: { LoginView() },
            label: {
                Text("")
            }
        )
    }
}
