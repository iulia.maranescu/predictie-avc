//
//  LoginViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Combine
import Resolver

class LoginViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var authenticationService: AuthenticationService

    // MARK: - Properties

    @Published var showError = false

    @Published var errorMessage = ""

    @Published var isLoading = false

    @Published var email: String = ""

    @Published var password: String = ""

    private var cancellables: Set<AnyCancellable> = []
}

// MARK: - Public

extension LoginViewModel {

    func onLogin() {
        isLoading = true
        authenticationService
            .loginUser(email: email, password: password)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        self.showError = true
                        self.errorMessage = "Parola introdusă nu este corectă."
                        self.isLoading = false
                    case .finished:
                        self.isLoading = false
                    }
                },
                receiveValue: { _ in }
            )
            .store(in: &cancellables)
    }

    func isButtonEnabled() -> Bool {
        !email.isEmpty && !password.isEmpty && email.count >= 6 && password.count >= 6
    }
}
