//
//  RegisterViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Combine
import Resolver

class RegisterViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var authenticationService: AuthenticationService

    // MARK: - Properties

    @Published var showError = false

    @Published var errorMessage = ""

    @Published var isLoading = false

    @Published var email: String = ""
    
    @Published var password: String = ""

    private var cancellables: Set<AnyCancellable> = []
}

// MARK: - Public

extension RegisterViewModel {

    func onRegister() {
        isLoading = true
        authenticationService
            .registerUser(email: email, password: password)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        self.showError = true
                        self.errorMessage = "În sistem există deja un cont asociat acestei adrese de email."
                        self.isLoading = false
                    case .finished:
                        self.isLoading = false
                    }
                },
                receiveValue: { _ in }
            )
            .store(in: &cancellables)
    }

    func isButtonEnabled() -> Bool {
        !email.isEmpty && !password.isEmpty && email.count >= 6 && password.count >= 6
    }
}
