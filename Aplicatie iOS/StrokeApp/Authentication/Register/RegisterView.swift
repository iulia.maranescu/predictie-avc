//
//  RegisterView.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Resolver

struct RegisterView: View {

    // MARK: - Dependencies

    @StateObject private var viewModel: RegisterViewModel = Resolver.resolve()

    // MARK: - Body

    var body: some View {
        ZStack {
            content
                .navigationBarTitleDisplayMode(.large)
                .navigationTitle("Înregistrare")
            if viewModel.isLoading {
                ProgressView()
            }
        }
        .alert(viewModel.errorMessage, isPresented: $viewModel.showError) {
            Button("OK", role: .cancel) { }
        }
    }

    private var content: some View {
        VStack(alignment: .center, spacing: 16) {
            Spacer().frame(height: 50)
            emailView
            passwordView
            HStack {
                Spacer()
                submitButton
                Spacer()
            }
            Spacer()
        }
        .padding(.horizontal, 32)
    }

    private var emailView: some View {
        TextField("Email", text: $viewModel.email)
            .textFieldStyle(.roundedBorder)
            .textInputAutocapitalization(.never)
    }

    private var passwordView: some View {
        SecureField("Parolă", text: $viewModel.password)
            .textFieldStyle(.roundedBorder)
    }

    private var submitButton: some View {
        Button(
            action: {
                viewModel.onRegister()
            }, label: {
                Text("Înregistrare")
                    .bold()
            }
        )
        .disabled(!viewModel.isButtonEnabled())
        .opacity(viewModel.isButtonEnabled() ? 1.0 : 0.3)
        .frame(width: 100)
        .padding()
        .background(.black)
        .foregroundColor(.white)
        .clipShape(Capsule())
    }
}
