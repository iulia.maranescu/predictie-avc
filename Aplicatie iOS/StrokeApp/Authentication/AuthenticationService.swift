//
//  AuthenticationService.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import Combine
import FirebaseAuth

protocol AuthenticationService {

    func isUserAuthenticated() -> AnyPublisher<Bool, Never>

    func currentUser() -> AnyPublisher<FirebaseAuth.User?, Error>

    func registerUser(email: String, password: String) -> Future<AuthDataResult, Error>

    func loginUser(email: String, password: String) -> Future<AuthDataResult, Error>

    func deauthenticate()
}

class RealAuthenticationService: AuthenticationService {

    // MARK: - Properties

    let authenticationStatus: CurrentValueSubject<Bool, Never> = .init(true)

    var cancellables: Set<AnyCancellable> = []

    // MARK: - Init

    init() {
        setAuthStatus()
    }

    // MARK: - AuthenticationRemoteDataSource

    func isUserAuthenticated() -> AnyPublisher<Bool, Never> {
        authenticationStatus.eraseToAnyPublisher()
    }

    func currentUser() -> AnyPublisher<FirebaseAuth.User?, Error> {
        return Just(Auth.auth().currentUser)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func registerUser(email: String, password: String) -> Future<AuthDataResult, Error> {
        Future<AuthDataResult, Error> { promise in
            Auth.auth().createUser(withEmail: email, password: password) { authDataResult, error in
                self.setAuthStatus()
                if let error = error {
                    promise(.failure(error))
                } else {
                    promise(.success(authDataResult!))
                }
            }
        }
    }

    func loginUser(email: String, password: String) -> Future<AuthDataResult, Error> {
        Future<AuthDataResult, Error> { promise in
            Auth.auth().signIn(withEmail: email, password: password) { authDataResult, error in
                self.setAuthStatus()
                if let error = error {
                    promise(.failure(error))
                } else {
                    promise(.success(authDataResult!))
                }
            }
        }
    }

    func deauthenticate() {
        do {
            try Auth.auth().signOut()
            setAuthStatus()
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
    }
}

private extension RealAuthenticationService {

    func setAuthStatus() {
        currentUser()
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { result in
                    guard let value = result else {
                        self.authenticationStatus.send(false)
                        return
                    }
                    self.authenticationStatus.send(true)
                }
            )
            .store(in: &cancellables)
    }
}
