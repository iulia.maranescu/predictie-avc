//
//  HTMLStringView.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

import WebKit
import SwiftUI

struct HTMLStringView: UIViewRepresentable {

    let htmlContent: String

    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
        uiView.loadHTMLString(headerString + htmlContent, baseURL: nil)
    }
}
