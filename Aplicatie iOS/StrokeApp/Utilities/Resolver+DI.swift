//
//  Resolver+DI.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import Resolver
import Foundation

extension Resolver {

    // MARK: - Public

    static func registerDependencies() {
        register { RealAuthenticationService() as AuthenticationService }.scope(.application)
        register { RealPredictionService() as PredictionService }

        register { StrokeAppViewModel() }
        register { RegisterViewModel() }
        register { LoginViewModel() }
        register { AuthenticationViewModel() }
        register { HomeViewModel() }
        register { MenuViewModel() }
        register { PredictViewModel() }
    }
}
