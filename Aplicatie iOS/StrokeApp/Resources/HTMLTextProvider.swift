//
//  HTMLTextProvider.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 05.03.2023.
//

struct HTMLTextProvider {

    func provideMenuText() -> String {
      """
        <h4>Această aplicație a fost dezvoltată de Maranescu Iulia. <br />Au fost utilizate următoarele tehnologii:</h4>
        <ul>
        <li><strong>Python</strong> pentru crearea modelului</li>
        <li><strong>ScikitLearn</strong> pentru transformarea modelului într-un model CoreML</li>
        <li><strong>Swift</strong> pentru dezvoltarea aplicației iOS</li>
        <li><strong>Firebase</strong> pentry autentificare și baza de date</li>
        </ul>
     """
    }
}
