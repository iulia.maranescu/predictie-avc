//
//  StrokeApp.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Resolver
import FirebaseCore

class AppDelegate: NSObject, UIApplicationDelegate {

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {
        Resolver.registerDependencies()
        FirebaseApp.configure()
        return true
    }
}

@main
struct StrokeApp: App {

    // MARK: - AppDelegate

    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate

    // MARK: - Depedencies

    @StateObject private var viewModel: StrokeAppViewModel = Resolver.resolve()

    var body: some Scene {
        WindowGroup {
            if viewModel.isAuthenticated {
                BaseTabView().tint(.black)
            } else {
                AuthenticationScreen().tint(.black)
            }
        }
    }
}
