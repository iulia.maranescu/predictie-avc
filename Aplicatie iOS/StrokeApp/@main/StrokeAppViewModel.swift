//
//  StrokeAppViewModel.swift
//  StrokeApp
//
//  Created by Maranescu Iulia on 04.03.2023.
//

import SwiftUI
import Combine
import Resolver

class StrokeAppViewModel: ObservableObject {

    // MARK: - Dependencies

    @Injected private var authenticationService: AuthenticationService

    // MARK: - Properties

    @Published var isAuthenticated = false

    private var cancellables: Set<AnyCancellable> = []

    // MARK: - Init

    init() {
        subscribeToPublishers()
    }
}

// MARK: - Public

extension StrokeAppViewModel {}

// MARK: - Private

private extension StrokeAppViewModel {

    func subscribeToPublishers() {
        authenticationService
            .isUserAuthenticated()
            .sink { [weak self] value in
                self?.isAuthenticated = value
            }
            .store(in: &cancellables)
    }
}
